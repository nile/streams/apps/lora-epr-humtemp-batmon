package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

public class HumTempBatmonPacketTest {

    private static final JsonElement DATA_FRAME =
            TestUtils.getDataAsJsonElement("xgD//wAACgglDGYB3wMAAAAAgAD3Aw0HAAMDAAEBACCY");

    @Test
    void givenDataFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        final Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME, HumTempBatmonPacket.class);
        assertEquals(255, packet.get("dummyByte"));
        assertEquals(991, packet.get("extwtdCnt"));
        assertEquals(0, packet.get("floodConnection1"));
        assertEquals(1, packet.get("floodConnection2"));
        assertEquals(1, packet.get("floodConnection3"));
        assertEquals(0, packet.get("floodReading1"));
        assertEquals(3, packet.get("floodReading2"));
        assertEquals(3, packet.get("floodReading3"));
        assertEquals(128, packet.get("humiCap0"));
        assertEquals(0, packet.get("humiSumPeriod"));
        assertEquals(3.31611328125, packet.get("mon33"));
        assertEquals(5.0096191406249995, packet.get("mon5"));
        assertEquals(198, packet.get("packetNumber"));
        assertEquals(0, packet.get("statusFlag"));
        assertEquals(255, packet.get("swbuild"));
        assertEquals(1.4542236328125, packet.get("temperatureAdcConv"));
        assertEquals(1.7783251231527093, packet.get("temperatureBridgeVoltage"));
        assertEquals(34542.6289157997, packet.get("temperatureGainAdcConv"));
        assertEquals(31.37578125, packet.get("deportedTemperatureAdcConv"));
        assertEquals(0, packet.get("temperatureDpConnection1"));
        assertEquals(1015, packet.get("tempGain"));
        assertEquals(1805, packet.get("tempRawValue"));
        assertEquals(38944, packet.get("deportedTempRawValue"));
        assertEquals(1.326714509544618, packet.get("vBat"));
    }
}
